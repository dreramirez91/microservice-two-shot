# Wardrobify

Team:

* Dre - Shoes
* Cougan - Hats

## Design

## Shoes microservice

The shoes microservice contains two main models, Shoe, and BinVO. Shoe has the following model fields: manufacturer, shoe model, color, picture url, and a foreign key to its shoe bin which associates to the BinVO. The BinVO in turn is used to associate a shoe with a specific bin within the wardrobe via polling. The only model field it needs to accomplish this is import href. 

## Hats microservice

The hats microservice contains two main models, Hat, and LocationVO. Hat has the following model fields: fabric, style name, color, picture url, and a foreign key to its location which associates to the LocationVO. The LocationVO in turn is used to associate a hat with a specific location within the wardrobe via polling. The only model field it needs to accomplish this is import href.
