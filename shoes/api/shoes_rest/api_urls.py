from django.urls import path, include
from .api_views import api_list_shoes, api_delete_shoe

urlpatterns = [
    path("", api_list_shoes, name="api_list_shoes"),
    path("<int:id>/delete/", api_delete_shoe, name="api_delete_shoe")
]
