from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from .models import Shoe, BinVO
from common.json import ModelEncoder


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "manufacturer", "shoe_model", "color", "picture_url"]
    
    def get_extra_data(self, o):
        return {"shoe_bin": o.shoe_bin.import_href}
    
    
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "manufacturer", "shoe_model", "color", "picture_url"]
    

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        all_shoes = Shoe.objects.all()
        return JsonResponse({"shoes": all_shoes}, encoder=ShoeListEncoder)
    else:
        content = json.loads(request.body)
        try:
            import_href = content["shoe_bin"]
            shoe_bin = BinVO.objects.get(import_href=import_href)
            content["shoe_bin"] = shoe_bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "This bin does not exist"},
                status=400
            )
        new_shoe = Shoe.objects.create(**content)
        return JsonResponse(
            new_shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
        
        
def api_delete_shoe(request, id):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
        
