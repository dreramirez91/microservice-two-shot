from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    
    def __str__(self):
        return self.import_href


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    shoe_model = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()
    shoe_bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    
    def __str__(self):
        return self.shoe_model
