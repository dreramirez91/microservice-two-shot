from django.contrib import admin
from .models import Shoe, BinVO

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = ["id", "manufacturer", "shoe_model", "color", "picture_url", "shoe_bin"]

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = ["import_href"]
