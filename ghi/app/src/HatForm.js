import React, {useState, useEffect} from 'react';

function HatForm () {
  const [locations, setLocations] = useState([])

  const [fabric, setFabric] = useState("")
  const [styleName, setStyleName] = useState("")
  const [color, setColor] = useState("")
  const [pictureUrl, setPictureUrl] = useState("")
  const [location, setLocation] = useState("")

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.fabric = fabric;
    data.style_name = styleName;
    data.color = color;
    data.picture_url = pictureUrl;
    data.location = location;
    console.log(data);

    const url = `http://localhost:8090/api/hats/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);

      setFabric("");
      setStyleName("");
      setColor("");
      setPictureUrl("");
      setLocation("");
    }
  }

  const handleFabricChange = (e) => {
    setFabric(e.target.value);
  }

  const handleStyleNameChange = (e) => {
    setStyleName(e.target.value);
  }

  const handleColorChange = (e) => {
    setColor(e.target.value);
  }

  const handlePictureUrlChange = (e) => {
    setPictureUrl(e.target.value);
  }

  const handleLocationChange = (e) => {
    setLocation(e.target.value)
  }

  useEffect(()=> {
    fetchData();
  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStyleNameChange} value={styleName} placeholder="Style Name" required name="style_name" id="style_name" className="form-control" />
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="Color" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture Url" required name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required name="location" className="form-select" id="location">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.href}>{location.id}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
