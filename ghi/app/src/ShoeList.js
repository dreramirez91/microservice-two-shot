import React, { useState, useEffect } from 'react'

export default function ShoeList() {
    const [shoes, setShoes] = useState([])
    async function fetchShoes() {
        const response = await fetch('http://localhost:8080/api/shoes/')
        console.log(response)
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setShoes(data.shoes)
        }
    }
    useEffect(() => {
        fetchShoes()
    }, [])
    
    async function delete_shoe(id) {
        const deleteUrl = `http://localhost:8080/api/shoes/${id}/delete/`
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(deleteUrl, fetchConfig)
        console.log(response)
        if (response.ok) {
            const response = await fetch('http://localhost:8080/api/shoes/')
            if (response.ok) {
                const data = await response.json()
                console.log(data)
                setShoes(data.shoes)
            }
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Image</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Model</th>
                    <th scope="col">Color</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td scope="row"><img src={shoe.picture_url} alt={`Image of ${shoe.color} ${shoe.manufacturer} ${shoe.model}`} className="img-thumbnail" style={{ maxWidth: '20em', maxHeight: '20em' }}/></td>
                            <td>{shoe.manufacturer}</td>
                            <td> {shoe.shoe_model}</td>
                            <td> {shoe.color}</td>
                            <td><button className="btn btn-danger" onClick={() => delete_shoe(shoe.id)}>Delete this shoe</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}
