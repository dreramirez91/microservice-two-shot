import React, { useState, useEffect } from 'react'

export default function ShoeForm() {
    const [manufacturer, setManufacturer] = useState('')
    const [shoeModel, setShoeModel] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [shoeBins, setShoeBins] = useState([])
    const [shoeBin, setShoeBin] = useState('')
    // const [formSuccess, setFormSuccess] = useState(false)
    // const [loadSuccess, setLoadSuccess] = useState(false)


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoeBins(data["bins"])
            // setLoadSuccess(true)
        }
    }
    function handleManufacturerChange(e) {
        setManufacturer(e.target.value)
    }
    function handleShoeModelChange(e) {
        setShoeModel(e.target.value)
    }
    function handleColorChange(e) {
        setColor(e.target.value)
    }
    function handlePictureUrlChange(e) {
        setPictureUrl(e.target.value)
    }
    function handleShoeBinsChange(e) {
        setShoeBins(e.target.value)
    }
    function handleShoeBinChange(e) {
        setShoeBin(e.target.value)
    }

    useEffect(() => {
        fetchData();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault()
        const data = {}
        data.manufacturer = manufacturer
        data.shoe_model = shoeModel
        data.color = color
        data.picture_url = pictureUrl
        data.shoe_bin = shoeBin
        console.log(data)
        const createShoeUrl = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(createShoeUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json()
            console.log(newShoe)
            setManufacturer('')
            setShoeModel('')
            setColor('')
            setPictureUrl('')
            setShoeBin('')
            // setFormSuccess(true)
        }
    }
    return(
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Brand" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Brand</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={shoeModel} onChange={handleShoeModelChange} placeholder="Model" required type="text" name="shoeModel" id="shoeModel" className="form-control" />
                    <label htmlFor="shoeModel">Model</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture URL" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" />
                    <label htmlFor="pictureUrl">Picture URL</label>
                </div>
                <div className="mb-3">
                                        <select value={shoeBin} onChange={handleShoeBinChange} name="shoeBin" id="shoeBin" className="form-select" required>
                                            <option value="">Choose a bin</option>
                                            {shoeBins.map(bin => {
                                                return (<option value={bin.href} key={bin.href}>{bin.id}</option>)
                                            })}
                                        </select>
                                    </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
    </div>
)
                                        }
