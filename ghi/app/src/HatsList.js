import React, { useState, useEffect } from 'react'

function HatsList() {
    const [hats, setHat] = useState([])
    async function fetchhats() {
        const response = await fetch('http://localhost:8090/api/hats/')
        console.log(response)
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setHat(data.hats)
        }
    }
    useEffect(() => {
        fetchhats()
    }, [])

    async function delete_hat(id) {
        const url = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const response = await fetch("http://localhost:8090/api/hats/")
            if (response.ok) {
                const data = await response.json()
                console.log(data)
                setHat(data.hats)
            }
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Image</th>
                    <th scope="col">Fabric</th>
                    <th scope="col">Style Name</th>
                    <th scope="col">Color</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td scope="row"><img src={hat.picture_url} alt={`Image of ${hat.fabric} ${hat.style_name} ${hat.color}`} className="img-thumbnail" style={{ maxWidth: '20em', maxHeight: '20em' }}/></td>
                            <td>{hat.fabric}</td>
                            <td> {hat.style_name}</td>
                            <td> {hat.color}</td>
                            <td><button className="btn btn-primary" onClick={() => delete_hat(hat.id)}>Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default HatsList;
